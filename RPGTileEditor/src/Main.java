import javax.swing.JFrame;

import tileeditor.ui.RPGTileEditorFrame;


public class Main {

	public static void main(String args[])
	{
		RPGTileEditorFrame frame = new RPGTileEditorFrame();
		frame.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frame.initialize();
		frame.initializeListeners();
		frame.pack();
		frame.setVisible(true);
		//last test
	}
	
}
