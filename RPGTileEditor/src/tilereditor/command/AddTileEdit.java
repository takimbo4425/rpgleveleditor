package tilereditor.command;


import java.awt.AlphaComposite;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.event.ActionEvent;
import java.awt.image.BufferedImage;

import javax.swing.AbstractAction;
import javax.swing.undo.AbstractUndoableEdit;

import tileeditor.components.Tile;
import tileeditor.util.State;

public class AddTileEdit extends AbstractUndoableEdit{
	
	private Tile _tile;
	private Tile _previousTile;
	private BufferedImage _map;
	private int _x;
	private int _y;
	private State _appState;
	
	public AddTileEdit(Tile tile, Tile previousTile, BufferedImage image, int x, int y, State appState)
	{
		_tile = tile;
		if(_previousTile != null)
		{
			System.out.println("PreviousTile should be null and its not");
		}
		
		_previousTile = (previousTile == null) ? null : previousTile.copy();
		_map = image;
		_x = x;
		_y = y;
		_appState = appState;
	}

	@Override
	public void redo()
	{
		Graphics2D mapGraphics = (Graphics2D) _map.getGraphics();
		if(_appState.overWrite)
		{
			mapGraphics.setComposite(AlphaComposite.Clear);
			mapGraphics.fillRect(_x, _y	, _tile.getTileImage().getWidth(), _tile.getTileImage().getHeight());
		}
		

		mapGraphics.drawImage(_tile.getTileImage(), _x, _y, null);
		mapGraphics.dispose();
	}
	
	@Override 
	public void undo()
	{
		Graphics2D mapGraphics = (Graphics2D) _map.getGraphics();
		if(_appState.overWrite)
		{
			mapGraphics.setComposite(AlphaComposite.Clear);
			mapGraphics.fillRect(_x, _y	, _tile.getTileImage().getWidth(), _tile.getTileImage().getHeight());
		}
		
		if(_previousTile != null)
		{
			System.out.println("Previous tile was not null.");
			mapGraphics.drawImage(_previousTile.getTileImage(), _x, _y, null);
		}
		else
		{
			//mapGraphics.setComposite(AlphaComposite.Clear);
			mapGraphics.setColor(Color.WHITE);
			System.out.println("Redrawing tile " + _x/32 + ", " + _y/32 + " with white");
			mapGraphics.fillRect(_x, _y	, _tile.getTileImage().getWidth(), _tile.getTileImage().getHeight());
			
		}
		
		mapGraphics.dispose();
	}
	
	@Override
	public boolean canRedo()
	{
		return true;
	}
	
	@Override
	public boolean canUndo()
	{
		return true;
	}
}
