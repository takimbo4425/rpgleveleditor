package tileeditor.ui;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.imageio.*;

import tileeditor.components.Tile;


public class TilePanel extends JPanel{

	String tileFile;
	
	public static int TILE_WIDTH = 32;
	public static int TILE_HEIGHT = 32;
	
	public int BUTTONS_PER_ROW = 3;
	
	public ArrayList<Tile> tileList;
	
	RPGTileEditorFrame parent = null;
	
	public TilePanel(RPGTileEditorFrame parent, String tileFileName)
	{
		super();
		this.parent = parent;
		this.tileFile = tileFileName;
		tileList = new ArrayList<Tile>();
		
		initialize();
		loadTiles();
		addTilesAsButtons();
	}
	
	public void initialize()
	{
		this.setLayout(new GridBagLayout());
	}
	
	public void loadTiles()
	{
		BufferedImage fullTileImage = null;
		
		try {
			
			fullTileImage = ImageIO.read(new File(tileFile));
			
		} catch (IOException e) {
			JOptionPane.showMessageDialog(this,"ERROR", "Unable to load tiles from " + tileFile, JOptionPane.ERROR_MESSAGE);
			e.printStackTrace();
			return;
		}
		
		for(int x = 0; x < fullTileImage.getWidth(); x = x + TILE_WIDTH)
		{
			//fullTileImage.getSubimage(x, 0, TILE_WIDTH - 1, TILE_HEIGHT - 1);
			Tile tile = new Tile(fullTileImage, x, 0, TILE_WIDTH, TILE_HEIGHT);
			tileList.add(tile);
		}
	}
	
	private void addTilesAsButtons()
	{
		GridBagConstraints gbc = new GridBagConstraints();
		gbc.insets = new Insets(4, 4, 4, 4);
		gbc.gridx = 0;
		gbc.gridy = 0;
		
		
		for(Tile tile : tileList)
		{
			JButton button = new JButton(new ImageIcon(tile.getTileImage()));
			button.setBorder(BorderFactory.createEmptyBorder());
			button.setContentAreaFilled(false);
			
			button.addActionListener(new TileButtonActionListener(tile));
			
			this.add(button, gbc);
			gbc.gridx++;
			
			if(gbc.gridx > BUTTONS_PER_ROW)
			{
				gbc.gridx = 0;
				gbc.gridy++;
			}
		}
	}
	
	private class TileButtonActionListener implements ActionListener
	{
		private Tile tile;
		
		public TileButtonActionListener(Tile tile)
		{
			this.tile = tile;
		}
		
		@Override
		public void actionPerformed(ActionEvent e) {
			parent.curTile = tile;
		}
		
	}
}
