package tileeditor.ui;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ComponentListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.image.BufferedImage;
import java.util.HashSet;

import javax.swing.JPanel;

import tileeditor.components.Tile;
import tileeditor.util.Pair;
import tileeditor.util.SelectTileSet;
import tileeditor.util.State;
import tilereditor.command.AddTileEdit;


public class MapPanel extends JPanel{
	
	private class MouseClickListener implements MouseListener, MouseMotionListener
	{

		private int previousXTile = -1;
		private int previousYTile = -1;
		private int count = 0;
		
		
		@Override
		public void mouseClicked(MouseEvent me) {
			Point p = me.getPoint();
			
			int xTilesOver = p.x / TilePanel.TILE_WIDTH * TilePanel.TILE_WIDTH;
			int yTilesOver = p.y / TilePanel.TILE_HEIGHT * TilePanel.TILE_HEIGHT;
			
			System.out.println(p);
			System.out.println("Should draw to tile " + xTilesOver/32 + ", " + yTilesOver/32);
			
			appendTile(parent.curTile, xTilesOver, yTilesOver);
			
		}

		@Override
		public void mouseDragged(MouseEvent me) {

			switch(parent.appState.tool)
			{
			case Selector:
				selectDrawDrawTileEvent(me);
				break;
			case Tile:
				mouseDragDrawTileEvent(me);
				break;
			default:
				break;
			}
		}

		@Override
		public void mouseMoved(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}

		@Override
		public void mousePressed(MouseEvent e) {
			if(parent.appState.tool == State.Mode.Selector)
			{
				parent.multiSelect = new SelectTileSet();
			}
		}

		@Override
		public void mouseReleased(MouseEvent e) {

		}
		
		public void mouseDragDrawTileEvent(MouseEvent me)
		{
			Point p = me.getPoint();
			
			int xTilesOver = p.x / TilePanel.TILE_WIDTH * TilePanel.TILE_WIDTH;
			int yTilesOver = p.y / TilePanel.TILE_HEIGHT * TilePanel.TILE_HEIGHT;
			
			if(xTilesOver == previousXTile && yTilesOver == previousYTile)
			{
				System.out.println("Mouse dragged, ignoring duplicate tile location " + previousXTile/32 + ", " + previousYTile/32);
				return;
			}
			
			System.out.println(p);
			System.out.println("Should draw to tile " + xTilesOver/32 + ", " + yTilesOver/32);
			
			appendTile(parent.curTile, xTilesOver, yTilesOver);
			
			previousXTile = xTilesOver;
			previousYTile = yTilesOver;
			count++;
			System.out.println("Tiles painted " + count);
		}
		
		private void selectDrawDrawTileEvent(MouseEvent me) {
			
			Point p = me.getPoint();
			
			int xTilesOver = p.x / TilePanel.TILE_WIDTH;
			int yTilesOver = p.y / TilePanel.TILE_HEIGHT;
			
			
			Point newTile = new Point(xTilesOver, yTilesOver);
			
			if(!parent.multiSelect.hasAStartTile())
			{
				parent.multiSelect.startTile = newTile;
			}
			parent.multiSelect.lastTile = newTile;
			
			for(int x = parent.multiSelect.startTile.x; x <= parent.multiSelect.lastTile.x; x++)
			{
				for(int y = parent.multiSelect.startTile.y; y <= parent.multiSelect.lastTile.y; y++)
				{
					System.out.println("Adding " + xTilesOver + ", " + yTilesOver + " to parent select set.");
					parent.multiSelect.add(new Point(x, y));
				}	
			}
			
			
			repaint();
		}
	}
	
	volatile BufferedImage map;
	volatile Tile tileArray[][];
	
	RPGTileEditorFrame parent = null;
	
	public MapPanel(RPGTileEditorFrame frame, int width, int height)
	{
		super();
		parent = frame;
		map = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
		tileArray = new Tile[width][height];
		
		for(int i = 0; i < width; i++)
		{
			for(int j = 0; j < height; j++)
			{
				map.setRGB(i, j, Color.WHITE.getRGB());
			}
		}
		setPreferredSize(new Dimension(width, height));
		MouseClickListener mcl = new MouseClickListener();
		this.addMouseListener(mcl);
		this.addMouseMotionListener(mcl);
	}
	
	public void appendTile(Tile tile, int x, int y)
	{
		if(tile != null)
		{
			Graphics mapGraphics = map.createGraphics();
			mapGraphics.drawImage(tile.getTileImage(), x, y, null);
			mapGraphics.dispose();
			
			AddTileEdit ate = new AddTileEdit(tile, tileArray[x/TilePanel.TILE_WIDTH][y/TilePanel.TILE_HEIGHT], map, x, y, parent.appState);
			parent.undoManager.addEdit(ate);
			tileArray[x/TilePanel.TILE_WIDTH][y/TilePanel.TILE_HEIGHT] = tile;
			this.repaint();
		}
	}
	
	@Override
	public void paint(Graphics g)
	{
		Graphics mapGraphics = map.getGraphics();
		mapGraphics.setColor(Color.BLACK);
		for(int w = 0; w < map.getWidth(); w = w + TilePanel.TILE_WIDTH)
		{
			mapGraphics.drawLine(w - 1, 0, w - 1, map.getHeight());
		}
		for(int h = 0; h < map.getHeight(); h = h + TilePanel.TILE_HEIGHT)
		{
			mapGraphics.drawLine(0, h - 1, map.getWidth(), h - 1);
		}
		mapGraphics.dispose();
		System.out.println("Redrawing");
		g.drawImage(map, 0, 0, map.getWidth(), map.getHeight(), null);
		
		if(parent.appState.tool == State.Mode.Selector)
		{
			g.setColor(new Color(0, 0, 1, 0.5f));
			for(Point tile : parent.multiSelect)
			{
				System.out.println("Drawing tile to  " + tile.x * TilePanel.TILE_WIDTH + ", " +  tile.y * TilePanel.TILE_HEIGHT );
				g.fillRect(tile.x * TilePanel.TILE_WIDTH, tile.y * TilePanel.TILE_HEIGHT, TilePanel.TILE_WIDTH - 1, TilePanel.TILE_HEIGHT - 1);
			}
		}
		
	}
}
