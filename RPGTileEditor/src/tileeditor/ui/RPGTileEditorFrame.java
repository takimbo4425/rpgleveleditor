package tileeditor.ui;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Image;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JToolBar;
import javax.swing.KeyStroke;
import javax.swing.event.UndoableEditEvent;
import javax.swing.event.UndoableEditListener;
import javax.swing.undo.UndoManager;
import javax.swing.undo.UndoableEditSupport;

import tileeditor.components.Tile;
import tileeditor.util.Pair;
import tileeditor.util.SelectTileSet;
import tileeditor.util.State;



public class RPGTileEditorFrame extends JFrame{

	private JPanel tileEditorPanel;
	private TilePanel tilePanel;
	private MapPanel mapPanel;
	
	public Tile curTile = null;
	public SelectTileSet multiSelect = new SelectTileSet();
	
	public State appState = new State();
	
	public UndoManager undoManager;
	public UndoableEditSupport undoSupport;
	
	public RPGTileEditorFrame()
	{
		super("TileMapEditor");
		
		tileEditorPanel = new JPanel();
		undoManager = new UndoManager();
		undoSupport = new UndoableEditSupport();
		undoSupport.addUndoableEditListener(new UndoAdapter());
	}
	
	public void initialize()
	{
		tileEditorPanel.setLayout(new BorderLayout());
		tileEditorPanel.setPreferredSize(new Dimension(800, 640));
		
		JToolBar toolBar = new JToolBar(JToolBar.HORIZONTAL);
		
		try {
			JButton selectButton = new JButton(new ImageIcon( ImageIO.read(new File("Assets/Buttons/cursor.png"))));
			selectButton.addActionListener(new ActionListener(){

				@Override
				public void actionPerformed(ActionEvent ae) {
					System.out.println("Setting app state to Select Mode");
					appState.tool = State.Mode.Selector;
				}
				
			});
			
			toolBar.add(selectButton);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tileEditorPanel.add(toolBar, BorderLayout.NORTH);
		
		tilePanel = new TilePanel(this, "Assets/WorldBaseTiles.png");	
		JScrollPane tileScroller = new JScrollPane(tilePanel);
		tileScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);
		tileScroller.setPreferredSize(new Dimension(6 * 32, 14 * 32));
		tileEditorPanel.add(tileScroller, BorderLayout.WEST);
		
		LevelSizeDialog dialog = new LevelSizeDialog(this);
		dialog.setVisible(true);
		if(!dialog.wasSuccessful())
		{
			System.exit(0);
		}
		
		Pair<Integer> levelDimension = dialog.getParams();
		mapPanel = new MapPanel(this, levelDimension._one, levelDimension._two);
		JScrollPane mapScroller = new JScrollPane(mapPanel);
		mapScroller.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED);
		mapScroller.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		
		tileEditorPanel.add(mapScroller);

		this.add(tileEditorPanel);
		
	}
	
	public void initializeListeners()
	{
		tileEditorPanel.getInputMap(JPanel.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).put(KeyStroke.getKeyStroke(KeyEvent.VK_Z,
                java.awt.event.InputEvent.CTRL_DOWN_MASK),
        "Undo");
		tileEditorPanel.getActionMap().put("Undo", new UndoAction());
	}
	
	private class UndoAction extends AbstractAction
	{

		@Override
		public void actionPerformed(ActionEvent arg0) {
			undoManager.undo();
			tileEditorPanel.repaint();
		}
		
	}
	
	private class CopyAction extends AbstractAction
	{
		@Override
		public void actionPerformed(ActionEvent ae)
		{
			
		}
	}
	private class UndoAdapter implements UndoableEditListener
	{

		@Override
		public void undoableEditHappened(UndoableEditEvent uee) {
			tileEditorPanel.repaint();
		}
		
	}
}
