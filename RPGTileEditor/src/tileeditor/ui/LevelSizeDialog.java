package tileeditor.ui;

import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import tileeditor.util.Pair;

public class LevelSizeDialog extends JDialog{
	
	private int _width = -1;
	private int _height = -1;
	
	private boolean _success = false;
	
	public LevelSizeDialog(JFrame parent)
	{
		super(parent, "Specify Level Size");
		this.setModal(true);
		this.initialize();
	}
	
	public void initialize()
	{
		this.setLayout(new GridBagLayout());
		GridBagConstraints gbc = new GridBagConstraints();
		
		gbc.fill = GridBagConstraints.BOTH;
		gbc.insets = new Insets(4, 4, 4, 4);
		gbc.weightx = 1.0;
		gbc.weighty = 1.0;
		
		JLabel widthLabel = new JLabel("Level width in px: ");
		gbc.gridx = 0;
		gbc.gridy = 0;
		this.add(widthLabel, gbc);
		
		final JTextField widthTextField = new JTextField(10);
		gbc.gridx = 1;
		gbc.gridy = 0;
		this.add(widthTextField, gbc);
		
		JLabel heightLabel = new JLabel("Level height in px: ");
		gbc.gridx = 0;
		gbc.gridy = 1;
		this.add(heightLabel, gbc);
		
		final JTextField heightTextField = new JTextField(10);
		gbc.gridx = 1;
		gbc.gridy = 1;
		this.add(heightTextField, gbc);
		
		JButton okButton = new JButton("Ok");
		okButton.addActionListener(new ActionListener(){

			@Override
			public void actionPerformed(ActionEvent arg0) 
			{
				
				try
				{
					_width = Integer.parseInt(widthTextField.getText());
					_height = Integer.parseInt(heightTextField.getText());
					_success = true;
					dispose();
				} 
				catch(NumberFormatException nfe)
				{
					_success = false;
				}
				
			}
			
		});
		gbc.gridx = 0;
		gbc.gridy = 2;
		this.add(okButton, gbc);
		
		JButton cancelButton = new JButton("Cancel");
		cancelButton.addActionListener(new ActionListener()
		{

			@Override
			public void actionPerformed(ActionEvent e) {
				_success = false;
				dispose();
			}
			
		});
		gbc.gridx = 1;
		gbc.gridy = 2;
		this.add(cancelButton, gbc);
		
		this.pack();
	}
	
	public Pair<Integer> getParams()
	{
		return new Pair<Integer>(_width, _height);
	}
	public boolean wasSuccessful()
	{
		return _success;
	}
}
