package tileeditor.util;


public class State {
	public enum Mode
	{
		Selector ,
		Tile
	}
	public boolean overWrite = false;
	public Mode tool = Mode.Tile;
}
