package tileeditor.util;

import java.awt.Point;
import java.util.HashSet;

public class SelectTileSet extends HashSet<Point>{
	private static final long serialVersionUID = 1L;

	public Point startTile;
	public Point lastTile;
	
	public SelectTileSet()
	{
		super();
		startTile = null;
		lastTile = null;
	}
	
	public boolean hasAStartTile()
	{
		return !(startTile == null);
	}

	public void clear()
	{
		startTile = null;
		lastTile = null;
	}
}
