package tileeditor.util;

public class Pair <T> {

	public T _one;
	public T _two;
	
	public Pair(T one, T two)
	{
		_one = one;
		_two = two;
	}
	
	@Override
	public int hashCode()
	{
		return (_one.toString() + _two.toString()).hashCode();
	}
}
