package tileeditor.components;

import java.awt.image.BufferedImage;


public class Tile {
	private BufferedImage sourceTileSheet;
	public int xPos;
	public int yPos;
	public int width;
	public int height;
	
	public Tile(BufferedImage tileSheet, int x, int y, int width, int height)
	{
		this.sourceTileSheet = tileSheet;
		this.xPos = x;
		this.yPos = y;
		this.width = width;
		this.height = height;
	}
	
	public BufferedImage getSourceTileSheet()
	{
		return sourceTileSheet;
	}
	
	public BufferedImage getTileImage()
	{
		return sourceTileSheet.getSubimage(xPos, yPos, width - 1, height - 1);
	}
	
	
	public Tile copy()
	{
		return new Tile(this.sourceTileSheet, new Integer(xPos), new Integer(yPos), new Integer(width), new Integer(height));
	}
}
